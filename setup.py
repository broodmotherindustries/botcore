from setuptools import setup, find_packages

setup(name='BotCore',
      version='0.0.1',
      description='Bot BotCore',
      url='',
      author='Code Iain',
      packages=find_packages(exclude=['contrib', 'docs', 'tests*']),
      install_requires=[
          'build_utils',
          'fake_rpi',
          'ruamel.yaml'
      ],
      zip_safe=False)

