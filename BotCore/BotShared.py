"""
Bot Shared this is for all shared functionality accross bots
"""
from pathlib import Path
import ruamel.yaml as yaml


class BotShared:
    """
    BotShared
    """

    def __init__(self, file_name=None):
        """
        init
        :param file_name: the file to load
        """
        self.details = {}

        if file_name is not None:
            self.config_file_name = file_name
        else:
            self.config_file_name = "config.yaml"

    def get_config(self):
        """
        loads the bots config
        :return: the config details
        """
        config_file = Path(self.config_file_name)
        if config_file.is_file():
            with open(config_file) as stream:
                try:
                    self.details = yaml.safe_load(stream)
                except yaml.YAMLError as exc:
                    print(exc)

    def create_config(self, details):
        """
        creates a config file
        :param details: the details to store in the config file
        :return: nothing
        """
        # TODO : re-look at this in the future as its not creating the yaml correctly.
        try:
            with open("test.yaml", "w") as stream:
                yaml.dump(details, stream)
                #stream.write("test")
        except yaml.YAMLError as ex:
            print(ex)
