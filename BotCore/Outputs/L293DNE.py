"""
L293DNE motor driver
"""
import sys
import importlib.util

if importlib.util.find_spec('RPi').name != 'fake_rpi.RPi':
    import RPi.GPIO as GPIO
else:
    import fake_rpi
    GPIO = fake_rpi.RPi.GPIO

    sys.modules['RPi'] = fake_rpi.RPi  # Fake RPi (GPIO)
    sys.modules['SMBUS'] = fake_rpi.smbus  # Fake SMBUS (I2C)


class L293DNE:
    """
    Basic Config layout
      Type: 'L293DNE'
      Connections:
        Motor1:
          Name: 'Left'
          pinA:  1
          pinB: 2
          pinE: 3
        Motor2:
          Name: 'Right'
          pinA:  4
          pinB: 5
          pinE: 6

    """
    motors = {}

    def __init__(self, connections, callback, mode=GPIO.BCM):
        GPIO.setmode(mode)
        for key, value in connections.items():
            if isinstance(value, dict):
                motor = Motor(value['pinA'], value['pinB'], value['pinE'])
                value.__setitem__('motor', motor)
                value.on_value_update += callback
                self.motors.__setitem__(value['Name'], value)


class Motor:
    """
    Motor Class
    """

    def __init__(self, pin_a, pin_b, pin_e):
        """
        init function
        :param pin_a: pin number
        :param pin_b: pin number
        :param pin_e: pin number
        """
        self.pin_a = pin_a
        self.pin_b = pin_b
        self.pin_e = pin_e

    def forward(self):
        """
        sets the motor forward
        :return:
        """
        GPIO.output(self.pin_a, GPIO.HIGH)
        GPIO.output(self.pin_b, GPIO.LOW)
        GPIO.output(self.pin_e, GPIO.HIGH)

    def backwards(self):
        """
        sets the motor backwards
        :return: nothing
        """
        GPIO.output(self.pin_a, GPIO.LOW)
        GPIO.output(self.pin_b, GPIO.HIGH)
        GPIO.output(self.pin_e, GPIO.HIGH)

    def stop(self):
        """
        stops the motor
        :return:
        """
        GPIO.output(self.pin_e, GPIO.LOW)
