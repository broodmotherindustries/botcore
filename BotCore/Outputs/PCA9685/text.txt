
def test():
    '''Servo driver test on channel 1'''
    import time
    a = Servo(1)
    a.setup()
    for i in range(0, 180, 5):
        print(i)
        a.write(i)
        time.sleep(0.1)
    for i in range(180, 0, -5):
        print(i)
        a.write(i)
        time.sleep(0.1)
    for i in range(0, 91, 2):
        a.write(i)
        time.sleep(0.05)
    print(i)


def install():
    all_servo = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    for i in range(16):
        all_servo[i] = Servo(i)
    for servo in all_servo:
        servo.setup()
        servo.write(90)


if __name__ == '__main__':
    import sys

    if len(sys.argv) == 2:
        if sys.argv[1] == "install":
            install()
    else:
        test()
