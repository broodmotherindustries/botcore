import sys
import importlib.util
try:
    importlib.util.find_spec('Rpi')
    found = True
    import RPi
    import smbus
except ImportError:
    found = False
    print ('not found')
    import fake_rpi
    sys.modules['RPi'] = fake_rpi.RPi  # Fake RPi (GPIO)
    sys.modules['SMBUS'] = fake_rpi.smbus  # Fake SMBUS (I2C)