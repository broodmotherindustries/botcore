from .PCA9685 import Servo
from Shared.EventHook import *


class SunfounderPCA9685:
    """
    Basic Config Layout
      Type: 'SunfounderPCA9685'
      Connections:
        ServoGroup1:
          Servo0:
            Name: 'front_left_hip'
            Value: 0
            Id: 0
          Servo1:
            Name: 'front_left_leg'
            Value: 0
            Id: 1
          Servo2:
            Name: 'front_left_foot'
            Value: 0
            Id: 2
        ServoGroup2:
          Servo0:
            Name: 'front_right_hip'
            Value: 0
            Id: 4
          Servo1:
            Name: 'front_right_leg'
            Value: 0
            Id: 5
          Servo2:
            Name: 'front_right_foot'
            Value: 0
            Id: 6
        ServoGroup3:
          Servo0:
            Name: 'back_left_hip'
            Value: 0
            Id: 8
          Servo1:
            Name: 'back_left_leg'
            Value: 0
            Id: 9
          Servo2:
            Name: 'back_left_foot'
            Value: 0
            Id: 10
        ServoGroup4:
          Servo0:
            Name: 'back_right_hip'
            Value: 0
            Id: 12
          Servo1:
            Name: 'back_right_leg'
            Value: 0
            Id: 13
          Servo2:
            Name: 'back_right_foot'
            Value: 0
            Id: 1
    """
    my_servo = {}

    def __init__(self, connections, callback):
        for key, v in connections.items():
            for subkey, v2 in v.items():
                servo = Servo.Servo(v2['Id'], bus_number=1)
                v2.__setitem__('servo', servo)
                v2.on_value_update += callback
                self.my_servo.__setitem__(v2['Name'], v2)
                servo.setup()

    def set_servo(self, name, value):
        if self.my_servo.__contains__(name):
            self.my_servo.get(name)['servo'].write(value)
            self.my_servo.get(name)['Value'] = value

    def get_servo(self, name):
        if self.my_servo.__contains__(name):
            return self.my_servo.get(name)['servo'].angle
