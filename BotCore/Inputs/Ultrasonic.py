import sys
import importlib.util
import threading

""" checks to see if the RPi library exists and if not it uses the fake"""
if importlib.util.find_spec('RPi').name != 'fake_rpi.RPi':
    found = True
    import RPi.GPIO as GPIO
else:
    found = False
    import fake_rpi
    GPIO = fake_rpi.RPi.GPIO

    sys.modules['RPi'] = fake_rpi.RPi  # Fake RPi (GPIO)
    sys.modules['SMBUS'] = fake_rpi.smbus  # Fake SMBUS (I2C)


import time
from Shared import EventHook


class Ultrasonic:

    def __init__(self, mode=GPIO.BCM, gpio_trigger=18, gpio_echo=24):
        GPIO.setmode(mode)
        self.distance_chaged = EventHook()
        self.GPIO_TRIGGER = gpio_trigger
        self.GPIO_ECHO = gpio_echo
        GPIO.setup(self.GPIO_TRIGGER, GPIO.OUT)
        GPIO.setup(self.GPIO_ECHO, GPIO.IN)
        self.monitor_in_background_thread = threading.Thread(target=self.monitor_distance)

    def get_distance(self):
        GPIO.output(self.GPIO_TRIGGER, True)
        # set Trigger after 0.01ms to LOW
        time.sleep(0.00001)
        GPIO.output(self.GPIO_TRIGGER, False)

        start_time = time.time()
        stop_time = time.time()

        # save start_time
        while GPIO.input(self.GPIO_ECHO) == 0:
            start_time = time.time()

        # save time of arrival
        while GPIO.input(self.GPIO_ECHO) == 1:
            stop_time = time.time()

        # time difference between start and arrival
        time_elapsed = stop_time - start_time
        # multiply with the sonic speed (34300 cm/s)
        # and divide by 2, because there and back
        distance = (time_elapsed * 34300) / 2

        return distance

    def monitor_in_background(self):
        self.monitor_in_background_thread.setName("UltraSonic_Thread")
        self.monitor_in_background_thread.start()

    def monitor_distance(self):
        last_distance = None
        while True:
            current_distance = self.get_distance()
            if last_distance is not None:
                if last_distance != current_distance:
                    self.distance_chaged.fire()
            last_distance = current_distance
