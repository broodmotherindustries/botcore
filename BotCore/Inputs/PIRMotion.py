import sys
import threading
import importlib.util

""" checks to see if the RPi library exists and if not it uses the fake"""
if importlib.util.find_spec('RPi').name != 'fake_rpi.RPi':
    found = True
    import RPi.GPIO as GPIO
else:
    found = False
    import fake_rpi
    GPIO = fake_rpi.RPi.GPIO

    sys.modules['RPi'] = fake_rpi.RPi  # Fake RPi (GPIO)
    sys.modules['SMBUS'] = fake_rpi.smbus  # Fake SMBUS (I2C)


from Shared import EventHook


class PIRMotion:

    def __init__(self, mode=GPIO.BCM, gpio_pin=11):
        self.detected = EventHook();
        self.gpio_pin = gpio_pin
        GPIO.setmode(mode)
        GPIO.setup(self.gpio_pin, GPIO.IN)
        self.monitor_thread = threading.Thread(target=self.monitor)
        self.monitor_thread.setName(name="PIR_Thread")
        self.monitor_thread.start()

    def monitor(self):
        while True:
            i = GPIO.input(self.gpio_pin)
            if i == 1:
                self.detected.fire()
