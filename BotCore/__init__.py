import sys
import importlib.util
""" checks to see if the RPi library exists and if not it uses the fake"""
if importlib.util.find_spec('RPi') != None:
    found = True
    import RPi
else:
    found = False
    import fake_rpi
    sys.modules['RPi'] = fake_rpi.RPi  # Fake RPi (GPIO)
    sys.modules['SMBUS'] = fake_rpi.smbus  # Fake SMBUS (I2C)


from .Outputs import *
from .Inputs import *

