# BroodMother Industries BotCore

This Library is designed to allow all bots that use it integration with different hardware

* Outputs
    * Sunfounder PCA 9685 16-Channel 12bit I2C Bus PWM Driver
    * L293DNE motor driver
* Inputs 
    * HC-SR04 Ultrasonic Sensor
    * PTRMotion Sensor
    

This Library will extend over time allowing access to additional hardware
  